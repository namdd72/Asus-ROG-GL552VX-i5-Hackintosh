# Asus-GL552VX-i5-Hackintosh


## Specification
```
- Name: Asus GL552VX
- Processor: Intel® Core™ i5 6300HQ
- Wifi: Qualcomm Atheros QCA9377 Wireless
- Audio: Conexant CX20751 
- Graphics: 
  * IGP: Intel HD Graphics 530
  * Discrete: NVIDIA GTX950m  
- Storage:
  * HDD: 1Tb HDD 5400RPM 
  * SSD: 1Tb Samsung QVO 860 
- Dual Boot:
  * Windows 10 Pro
  * MacOS Mojave
```

## Known work
```
1. Keyboard (include blk)
2. Brightness
3. Audio (Patch Apple HDA)
4. Trackpad
5. FN keys
```

## Known not work
```
1. Discrete Graphics: NVIDIA GTX950m(Disabled - MacOS not supported optimus)
2. Built-in Wifi (Must be replaced)
```

Patch Clover from: https://github.com/xuanquydsr/Gl552VX-Mojave


